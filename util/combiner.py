from datetime import timedelta
from pathlib import Path

import numpy as np
from dateutil.parser import parse
from netCDF4 import Dataset

from util.shape_shifter import new_dimensions, new_attributes, transpose_and_clip


class Clip:
    """Class for holding start/end row and column indexes."""

    def __init__(self, ns, ne, ms, me, xcount, ycount):
        self.ns = ns
        self.ne = ne
        self.ms = ms
        self.me = me
        self.xcount = xcount
        self.ycount = ycount


class Combiner:
    """Class for helping combine multiple netcdf data into a single netcdf file."""

    def __init__(self):
        self.nc = None
        self.reference_time = None
        self.clip = None
        self.cur_time_index = -1
        self.time_count = 0
        self.files = []
        self.messages = []

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def open(self, file_path):
        if self.nc is not None:
            return self

        p = Path(file_path)
        if p.exists():
            p.unlink()

        self.nc = Dataset(file_path, 'w')

        return self

    def close(self):
        if self.nc is not None:
            self.nc.close()
            self.nc = None

    def included_dataset(self, var, datasets):
        if datasets is None:
            return True

        if 'time' not in var.dimensions:
            return True

        return var.name in datasets

    def clip_indexes(self, file, extent):
        ns, ne, ms, me = 0, -1, 0, -1
        xcount = 0
        ycount = 0
        with Dataset(file, 'r') as nc:
            for var_name, var in nc.variables.items():
                if var_name == 'lon':
                    if extent is not None:
                        indexes = np.where((var[:] >= extent.xmin) & (var[:] <= extent.xmax))
                        if not indexes:
                            raise Exception('No values fall within clip extent')
                        ms = indexes[0][0]
                        me = indexes[0][-1]
                    else:
                        me = var.shape[0] - 1
                    xcount = len(var[ms:me+1])
                elif var_name == 'lat':
                    if extent is not None:
                        indexes = np.where((var[:] >= extent.ymin) & (var[:] <= extent.ymax))
                        if not indexes:
                            raise Exception('No values fall within clip extent')
                        ns = indexes[0][0]
                        ne = indexes[0][-1]
                    else:
                        ne = var.shape[0] - 1
                    ycount = len(var[ns:ne + 1])

        return Clip(ns, ne, ms, me, xcount, ycount)


    def write_header(self, template_file, time_count, reference_time, datasets=None, clip_extent=None):
        self.reference_time = reference_time
        self.time_count = time_count
        self.clip = self.clip_indexes(template_file, clip_extent)
        with Dataset(template_file, 'r') as nc:
            attr = {'title': 'Gridded Rainfall', 'source': 'TUFLOW 2016 or later',
                    'references': 'TUFLOW NetCDF Rainfall Format (http://wiki.tuflow.com/index.php?title=TUFLOW_NetCDF_Rainfall_Format)',
                    'comment': 'Contains time-carying gridded rainfall for TUFLOW simulation'}
            attr = dict(**attr, **{x: nc.getncattr(x) for x in nc.ncattrs()})
            self.nc.setncatts(attr)
            for _, dim in nc.dimensions.items():
                if dim.name == 'time':
                    self.nc.createDimension(dim.name, time_count)
                elif dim.name == 'lon':
                    self.nc.createDimension('x', self.clip.xcount)
                elif dim.name == 'lat':
                    self.nc.createDimension('y', self.clip.ycount)
                else:
                    self.nc.createDimension(dim.name, dim.size)

            if 'time' not in nc.dimensions:
                self.nc.createDimension('time', time_count)

            # add time manually
            new_var = self.nc.createVariable('time', np.float64, ('time'), compression='zlib')
            attr = {'standard_name': 'time', 'long_name': 'time', 'units': 'hours', 'axis': 'T'}
            new_var.setncatts(attr)

            for _, var in nc.variables.items():
                if var.name == 'time' or not self.included_dataset(var, datasets):
                    continue
                if var.name == 'lat' or var.name == 'lon':
                    var_name = 'x' if var.name == 'lon' else 'y'
                    pass
                elif var.name in datasets:
                    var_name = 'rainfall_depth'
                else:
                    var_name = var.name
                dimensions, index = new_dimensions(var)
                attr = new_attributes(var, index)
                new_var = self.nc.createVariable(var_name, var.datatype, dimensions, compression='zlib')
                new_var.setncatts(attr)

    def add_data(self, input_file, time, datasets=None):
        self.files.append(input_file)
        with Dataset(input_file, 'r') as nc:

            if 'time' not in nc.variables:
                self.messages.append(f'Time is not a variable in {input_file}. Skipping file....')
                return

            var = nc.variables['time']
            if 'units' in var.ncattrs():
                units = var.units
            elif 'Units' in var.ncattrs():
                units = var.Units
            elif 'UNITS' in var.ncattrs():
                units = var.UNITS
            else:
                self.messages.append('Cannot find time units in {input_file}. Skipping....')
                return

            self.cur_time_index += 1
            rt = parse(units.strip('seconds since '), ignoretz=True)
            date = rt + timedelta(seconds=int(nc.variables['time'][:][0]))
            hours = (date - self.reference_time).total_seconds() / 3600.
            self.nc.variables['time'][self.cur_time_index] = hours
            timestep = 0.
            if self.cur_time_index > 0:
                timestep = self.nc.variables['time'][self.cur_time_index] - self.nc.variables['time'][self.cur_time_index-1]

            for _, var in nc.variables.items():
                if var.name == 'time' or not self.included_dataset(var, datasets):
                    continue

                if var.name not in ['lon', 'lat'] and var.name not in datasets and var.name not in self.nc.variables:
                    self.messages.append(f'Could not add dataset {var.name} from {input_file}')
                    continue
                elif var.name in ['lat', 'lon']:
                    var_name = 'x' if var.name == 'lon' else 'y'
                elif var.name in datasets:
                    var_name = 'rainfall_depth'
                else:
                    var_name = var.name

                dims, index = new_dimensions(var)
                try:
                    data = transpose_and_clip(var, index, self.clip)
                    i = None
                    if 'time' in var.dimensions:
                        i = var.dimensions.index('time')
                    if i is None and self.cur_time_index == 0:
                        self.nc.variables[var_name][:] = data[:]
                    elif i == 0 and (datasets is None or var.name in datasets):
                        if var.name in datasets:
                            factor = timestep
                        else:
                            factor = 1.
                        self.nc.variables[var_name][self.cur_time_index,:] = data[0:,] * factor
                    elif i == 1 and (datasets is None or var.name in datasets):
                        if var.name in datasets:
                            factor = timestep
                        else:
                            factor = 1.
                        for j in range(nc.shape[0]):
                            self.nc.variables[var_name][j,self.cur_time_index,:] = data[j,0,:] * factor
                except Exception as e:
                    self.messages.append(str(e))


class Merger:
    """Class for merging data from a reprojected netcdf grid into an existing netcdf."""

    def __init__(self):
        self.messages = []

    def merge(self, og_file, reproj_file, output_file):
        with Dataset(output_file, 'w') as nc_out:
            with Dataset(og_file, 'r') as nc1:
                with Dataset(reproj_file, 'r') as nc2:
                    attr = {x: nc1.getncattr(x) for x in nc1.ncattrs()}
                    nc_out.setncatts(attr)

                    nc_out.createDimension('x', size=nc2.dimensions['x'].size)
                    nc_out.createDimension('y', size=nc2.dimensions['y'].size)
                    nc_out.createDimension('time', size=nc1.dimensions['time'].size)

                    var = nc_out.createVariable('x', nc2.variables['x'].datatype, ('x'), compression='zlib')
                    attr = {x: var.getncattr(x) for x in var.ncattrs()}
                    var.setncatts(attr)
                    var = nc_out.createVariable('y', nc2.variables['y'].datatype, ('y'), compression='zlib')
                    attr = {x: var.getncattr(x) for x in var.ncattrs()}
                    var.setncatts(attr)
                    var = nc_out.createVariable('time', nc1.variables['time'].datatype, ('time'), compression='zlib')
                    attr = {x: var.getncattr(x) for x in var.ncattrs()}
                    var.setncatts(attr)
                    var = nc_out.createVariable('rainfall_depth', np.float64, ('time', 'y', 'x'), compression='zlib')
                    attr = {x: var.getncattr(x) for x in var.ncattrs()}
                    var.setncatts(attr)

                    nc_out.variables['x'][:] = nc2.variables['x'][:]
                    nc_out.variables['y'][:] = nc2.variables['y'][:]
                    nc_out.variables['time'][:] = nc1.variables['time'][:]

                    for i, _ in enumerate(nc1.variables['time']):
                        band = i + 1
                        nc_out.variables['rainfall_depth'][i,:] = nc2.variables[f'Band{band}'][:]
                        if i == 0:
                            if 'grid_mapping' in nc2.variables[f'Band{band}'].ncattrs():
                                crs_var_name = nc2.variables[f'Band{band}'].grid_mapping
                                nc_out.variables['rainfall_depth'].grid_mapping = crs_var_name
                                if crs_var_name in nc2.variables:
                                    var = nc_out.createVariable(crs_var_name, nc2.variables[crs_var_name].datatype)
                                    attr = {x: nc2.variables[crs_var_name].getncattr(x) for x in nc2.variables[crs_var_name].ncattrs()}
                                    var.setncatts(attr)
