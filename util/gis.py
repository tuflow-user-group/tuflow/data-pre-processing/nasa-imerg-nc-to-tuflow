from osgeo import ogr, gdal


class Rectangle:

    def __init__(self, xmin, xmax, ymin, ymax):
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax


def get_driver_by_extension(ext, driver_type):
    if not ext:
        return

    ext = ext.lower()
    if ext[0] == '.':
        ext = ext[1:]

    for i in range(gdal.GetDriverCount()):
        drv = gdal.GetDriver(i)
        md = drv.GetMetadata_Dict()
        if ('DCAP_RASTER' in md and driver_type == 'gdal') or ('DCAP_VECTOR' in md and driver_type == 'ogr'):
            if not drv.GetMetadataItem(gdal.DMD_EXTENSIONS):
                continue
            driver_extensions = drv.GetMetadataItem(gdal.DMD_EXTENSIONS).split(' ')
            for drv_ext in driver_extensions:
                if drv_ext.lower() == ext:
                    return drv.ShortName


def get_extent(gis_file):
    if not gis_file:
        return

    driver_name = get_driver_by_extension(gis_file.suffix, 'ogr')
    if driver_name is None:
        raise Exception(f'Unrecognised GIS format: {gis_file.suffix}')

    ds = ogr.GetDriverByName(driver_name).Open(str(gis_file))
    if ds is None:
        raise Exception(f'Could not open file: {gis_file}')

    lyr = ds.GetLayerByName(gis_file.stem)
    if lyr is None:
        raise Exception(f'Could not open layer: {gis_file.stem}')

    xmin, xmax, ymin, ymax = 9e29, -9e29, 9e29, -9e29
    for feat in lyr:
        geom = feat.geometry()
        for ring in geom:
            for point in ring.GetPoints():
                x, y = point
                xmin = min(xmin, x)
                xmax = max(xmax, x)
                ymin = min(ymin, y)
                ymax = max(ymax, y)

            ring = None
        feat = None

    ds, lyr = None, None

    return Rectangle(xmin, xmax, ymin, ymax)


def reproject(src, dst, src_srs, dest_srs):
    src = str(src)
    dst = str(dst)
    ds = gdal.Open(rf'NETCDF:{src}:rainfall_depth"')
    gdal.Warp(dst, ds, options=['-s_srs', f'EPSG:{src_srs}', '-t_srs', f'{dest_srs}', '-r', 'bilinear'])