import re

import numpy as np


def transpose_and_clip(var, i, clip=None):
    if len(var.dimensions) > 4:
        raise NotImplementedError('Data with more than 4 dimensions is not supported yet.')


    if clip is not None:
        if i is None and len(var.dimensions) == 1 and ('lat' in var.dimensions or 'lon' in var.dimensions):
            i1, i2 = (clip.ms, clip.me) if 'lon' in var.dimensions else (clip.ns, clip.ne)
            return var[i1:i2+1]
        elif i is None and len(var.dimensions) == 2 and ('lat' in var.dimensions or 'lon' in var.dimensions):
            lon_ind = var.dimensions.index('lon') if 'lon' in var.dimensions else None
            lat_ind = var.dimensions.index('lat') if 'lat' in var.dimensions else None
            if lon_ind is not None and lat_ind is not None:
                if lon_ind == 0:
                    return var[clip.ms:clip.me+1,clip.ns:clip.ne+1]
                else:
                    return var[clip.ns:clip.ne + 1, clip.ms:clip.me + 1]
            else:
                i1, i2 = (clip.ms, clip.me) if lon_ind is not None else (clip.ns, clip.ne)
                if lon_ind == 0 or lat_ind == 0:
                    return var[i1:i2+1,:]
                else:
                    return var[:,i1:i2+1]
        elif i is None and 'lat' not in var.dimensions and 'lon' not in var.dimensions:
            return var[:]
        elif i is None:
            raise Exception(f'not sure what this is: {var.dimensions}')
    elif i is None:
        return var[:]

    if len(var.dimensions) == 2:
        return np.transpose(var[:])

    if len(var.dimensions) == 3:
        if i != 1:
            raise ValueError('Cannot convert 3dim array where the index for x or lon dimension is not at index 1')
        if clip is not None:
            a = np.zeros((var.shape[0], clip.ycount, clip.xcount))
            for j in range(var.shape[0]):
                a[j,:,:] = np.transpose(var[j,clip.ms:clip.me+1,clip.ns:clip.ne+1])
        else:
            a = np.zeros(reorder(var.shape, i))
            for j in range(var.shape[0]):
                a[j,:] = np.transpose(var[j,:])

        return a

    if len(var.dimensions) == 4:
        if i != 2:
            raise ValueError('Cannot convert 4dim array where the index for x or lon dimension is not at index 2')
        if clip is not None:
            a = np.zeros((var.shape[0], var.shape[1], clip.ycount, clip.xcount))
            for j in range(var.shape[0]):
                for k in range(var.shape[1]):
                    a[j,k,:,:] = np.transpose(var[j,k,clip.ms:clip.me+1,clip.ns:clip.ne+1])
        else:
            a = np.zeros(reorder(var.shape, i))
            for j in range(var.shape[0]):
                for k in range(var.shape[1]):
                    a[j,k,:] = np.transpose(var[j,k,:])

        return a


def reorder(array, i):
    return array[:i] + array[i+1:i+2] + array[i:i+1] + array[i+2:]


def new_dimensions(var):
    for i in range(len(var.dimensions)):
        if ('lon' in var.dimensions or 'lat' in var.dimensions) and 'time' not in var.dimensions:
            s = ','.join(var.dimensions)
            s = re.sub(r'lon((?=\W)|$)', 'x', s)
            s = re.sub(r'lat((?=\W)|$)', 'y', s)
            s = s.split(',')
            return s, None
        if i == len(var.dimensions) - i:
            continue
        if var.dimensions[i:i+2] == ('lon', 'lat') or var.dimensions[i:i+2] == ('x', 'y'):
            s = (','.join(var.dimensions).replace('lon', 'x').replace('lat', 'y')).split(',')
            return reorder(s, i), i

    return var.dimensions, None


def new_attributes(var, i):
    attr = {x: var.getncattr(x) for x in var.ncattrs()}
    if i is None:
        return attr

    dim_names = attr.get('DimensionNames')
    if dim_names is None:
        return attr

    dim_names_lst = [x.strip() for x in dim_names.split(',')]
    new_dim_names_lst = reorder(dim_names_lst, i)
    attr['DimensionNames'] = ','.join(new_dim_names_lst)
    return attr
