# nasa-imerg-nc-to-tuflow

Script to convert NetCDF rainfall grids downloaded from NASA's IMERG Precipitation Measurement portal into TUFLOW 
compatible NetCDF.

## Quickstart

1. Clone repository `git clone https://gitlab.com/tuflow-user-group/tuflow/data-pre-processing/nasa-imerg-nc-to-tuflow.git`
2. Update inputs in `nasa_imerg_nc_to_tuflow.py`
    - INPUT - list with 2 inputs `[/path/to/input-grids-with-wilcard, /path/to/shapefile-containing-trim-polygon]`
    - DATETIME_FORMAT - string containing datetime format in file name
    - OUTPUT - /path/to/output-netcdf
    - DATASET - name of dataset inside netcdf files containing rainfall data
    - SRS - string with output CRS (projection)
3. Run script

## Details

In order to convert the input NetCDF grids, the following steps are undertaken by the script:
* Transpose the shape of the data from (time,x,y) to (time,y,x)
* Concatenate all input grids into a single time varying NetCDF grid
* Convert units from mm/hr to 'depth [mm]' (since previous timestep)
* Clip the data to the input polygon
* Reproject data from spherical to cartesian coordinates

## Example

See current input variables in `nasa_imerg_nc_to_tuflow.py`. The name of the input files are as follows:
* inputs\3B-HHR.MS.MRG.3IMERG.20170910-S053000-E055959.0330.V06B.HDF5.nc4
* inputs\3B-HHR.MS.MRG.3IMERG.20170910-S060000-E062959.0360.V06B.HDF5.nc4
* inputs\3B-HHR.MS.MRG.3IMERG.20170910-S063000-E065959.0390.V06B.HDF5.nc4
* inputs\3B-HHR.MS.MRG.3IMERG.20170910-S073000-E075959.0450.V06B.HDF5.nc4

## License

MIT License

## Changelog

 - 0.3 fixes units - should be mm (not m)
 - 0.2 fixes final combining of reprojected datasets and TUFLOW formatted NetCDF
 - 0.1 initial commit