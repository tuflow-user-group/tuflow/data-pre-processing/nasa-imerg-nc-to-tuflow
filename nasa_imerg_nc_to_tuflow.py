import os
import re
from pathlib import Path
from datetime import datetime

from util.combiner import Combiner, Merger
from util.gis import get_extent, reproject


__VERSION__ = 0.3


INPUT = [r'.\inputs\*.nc4', r".\inputs\extent_polygon.shp"]  # input files and clip polygon
DATETIME_FORMAT = '%Y%m%d-S%H%M%S'  # date format in file name
OUTPUT = r'.\noaa_to_tuflow.nc'  # output NetCDF
DATASET = 'precipitationCal'  # dataset in NetCDF input files containing rainfall data
SRS = 'ESRI:102361'  # projection of output file


def main():
    time_start = datetime.now()  # start time for recording script speed
    
    # get all the input files - expand wildcards
    file_glob, gis_file = INPUT
    if gis_file:
        gis_file = Path(gis_file)
    dir = Path(file_glob).parent
    glob = Path(file_glob).name
    files = [x for x in dir.glob(glob)]
    if not files:
        raise Exception('Could not find any input files')

    # strip the time from the input file names to create a datetime time series
    try:
        times = [datetime.strptime(re.findall(r'\d{8}-S\d{6}', str(x))[0], DATETIME_FORMAT) for x in files]
    except IndexError as e:
        times = []
        for file in files:
            if not re.findall(r'\d{8}-S\d{6}', str(file)):
                raise Exception(f'Could not find datetime string in file name: {file.name}')
    except ValueError as e:
        times = []
        raise Exception(f'Could not extract datetime string from file name: {e}')

    # input clip polygon extent
    extent = get_extent(gis_file)

    # sort input files by date
    d = {str(f): t for f, t in zip(files, times)}
    files.sort(key=lambda x: d[str(x)])
    times.sort()
    files.insert(0, files[0])  # shift grid timesteps so they align with TUFLOW format (depth since last timestep)
    times.append(times[-1])
    
    # initialise combiner (used for combining all input netcdf files into a single output netcdf)
    p = Path(OUTPUT)
    tmp1 = p.parent / (p.stem + 'tmp1.nc')
    combiner = Combiner()
    with combiner.open(tmp1) as fo:
        # write netcdf header
        time_count = len(files[:-1])
        fo.write_header(files[0], time_count=time_count, reference_time=times[0], datasets=[DATASET], clip_extent=extent)
        
        # initialise progress bar
        prog_prev = -1
        prog_milestone = 0
        for i, (file, time) in enumerate(zip(files[:-1], times[:-1])):
            # progress bar stuff
            prog = int(i / (time_count - 1) * 100)
            if prog != prog_prev:
                if prog >= prog_milestone:
                    print(prog_milestone, end='', flush=True)
                    prog_milestone += 10
                else:
                    print('.', end='', flush=True)
                prog_prev = prog

            # add input netcdf data into output netcdf
            fo.add_data(file, time, datasets=[DATASET])

    # check if any errors or warnings
    if len(combiner.messages) > 0:
        print('\n\nErrors or Warnings occured:\n{0}'.format('\n'.join(combiner.messages)))

    # reproject data
    print('\nReprojecting data')
    tmp2 = p.parent / (p.stem + 'tmp2.nc')
    reproject(tmp1, tmp2, 4326, SRS)
    
    # merger class used to merge reprojected data into output netcdf
    print(f'Merging reprojected data into {OUTPUT}')
    merger = Merger()
    merger.merge(str(tmp1), str(tmp2), OUTPUT)

    # try deleting tmp files
    try:
        os.remove(tmp1)
    except:
        pass
    try:
        os.remove(tmp2)
    except:
        pass

    time_end = datetime.now()
    print(f'Script took {(time_end - time_start).total_seconds()} seconds to run')


if __name__ == '__main__':
    main()
